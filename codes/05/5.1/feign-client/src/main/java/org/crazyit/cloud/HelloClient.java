package org.crazyit.cloud;

import feign.RequestLine;

/**
 * 客户端调用的服务接口
 * @author 杨恩雄
 *
 */
public interface HelloClient {

	  @RequestLine("GET /hello")
	  String sayHello();
}
